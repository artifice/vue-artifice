module.exports = {
    root: true,
    env: {
        node: true,
    },
    extends: [
        'plugin:vue/recommended',
        'eslint/recommended',
    ],
    parserOptions: {
        parser: 'babel-eslint',
    },
    rules: {
        'no-unused-vars': 'off',
        'no-mixed-spaces-and-tabs': 0,
        'import/no-extraneous-dependencies': 0,
    },
    overrides: [
        {
            files: [
                '**/__tests__/*.{j,t}s?(x)',
                '**/tests/unit/**/*.spec.{j,t}s?(x)',
                '**/src/**/*.test.{j,t}s?(x)',
            ],
            env: {
                jasmine: true,
                jest: true,
            },
        },
    ],
};

<div align="center">
  <a href="https://www.npmjs.com/package/vue-artifice"><img src="https://img.shields.io/npm/v/vue-artifice?color=brightgreen&style=for-the-badge" alt="Version"/></a>
  <a href="https://www.npmjs.com/package/vue-artifice"><img src="https://img.shields.io/npm/l/vue-artifice?style=for-the-badge" alt="License"/></a>
</div>

# Vue Artifice UI Components

> UI components build for [Vue.js](https://vuejs.org) 2.x

This is a collection of components especifically suited for Artifice eCommerce package.

## Installation

Install the `vue-artifice` package:

```
npm install vue-artifice
```

## Usage

Install the Vue plugin:

```js
import Vue from 'vue';
import VueArtifice from 'vue-artifice';

Vue.use(VueArtifice);
```

**Documentation is on Work In Progress**

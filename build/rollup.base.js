import vue from 'rollup-plugin-vue';
import commonjs from '@rollup/plugin-commonjs';
import buble from '@rollup/plugin-buble';

export default {
    input: 'src/index.js',
    plugins: [
        vue({
            css: true,
            compileTemplate: true,
        }),
        commonjs(),
        buble(),
    ],
    watch: {
        include: 'src/**',
    },
    external: [
        'vue',
        'lodash',
    ],
};

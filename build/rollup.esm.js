import base from './rollup.base';

const config = Object.assign({}, base, {
    output: {
        name: 'VueArtifice',
        file: 'dist/vue-artifice.esm.js',
        format: 'es',
        sourcemap: true,
    },
    external: [
        ...base.external,
        'lodash'
    ],
});

export default config;

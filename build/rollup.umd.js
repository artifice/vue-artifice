import base from './rollup.base';

const config = Object.assign({}, base, {
    output: {
        exports: 'named',
        name: 'vue-artifice',
        file: 'dist/vue-artifice.umd.js',
        format: 'umd',
        sourcemap: true,
    },
});

export default config;

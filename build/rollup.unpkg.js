import base from './rollup.base';
import { terser } from 'rollup-plugin-terser';

const config = Object.assign({}, base, {
    output: {
        exports: 'named',
        name: 'VueArtifice',
        file: 'dist/vue-artifice.min.js',
        format: 'iife',
        sourcemap: true,
        globals: {
            vue: 'Vue',
        },
    },
});

config.plugins.push(terser());

export default config;

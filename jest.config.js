module.exports = {
    preset: '@vue/cli-plugin-unit-jest',
    testMatch: [
        '**/tests/*.test.js'
    ],
    collectCoverage: true,
    collectCoverageFrom: [
        '<rootDir>/src/components/*.vue',
    ],
};

export { default as Input } from './components/Input.vue';
export { default as Table } from './components/Table.vue';
// upload
export { default as Select } from './components/Select.vue';
export { default as Toggle } from './components/Toggle.vue';
export { default as Tabs } from './components/Tabs.vue';
export { default as Tab } from './components/Tab.vue';
export { default as Modal } from './components/Modal.vue';
// alert
// confirm
export { default as Dropdown } from './components/Dropdown.vue';

import * as artifice from '../src/install';

describe('install', () => {
    function Vue() {
        // do nothing
    }

    Vue.component = (key) => {
        // console.log(key);
    };

    let compSpy;

    beforeEach(() => {
        compSpy = jest.spyOn(Vue, 'component');
    });

    it('should be able to install with prefix', () => {
        // simulate a vue.use
        artifice.install(Vue, { prefix: 'Artifice' });
    });
})
